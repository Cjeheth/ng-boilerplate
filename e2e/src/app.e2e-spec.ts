import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should navigate to a page', (done: Function) => {
    page
      .navigateTo()
      .then(() => done())
      .catch(() => done(new Error('Failed to navigate to a page')));
  });
});
