import { OnDestroy } from '@angular/core';
import { MonoTypeOperatorFunction, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const metaProperty = 'destroy$';

export function takeUntilDestroyed<T>(component: OnDestroy): MonoTypeOperatorFunction<T> {
  let destroy$: Subject<void> = component[metaProperty];

  if (!destroy$) {
    destroy$ = new Subject();

    const originalDestructor = component['ngOnDestroy'];
    component['ngOnDestroy'] = () => {
      if (originalDestructor) {
        originalDestructor.call(component);
      }

      destroy$.next();
      destroy$.complete();
    };
  }

  return takeUntil(destroy$);
}
