import { AnimationEvent } from '@angular/animations';
import { ChangeDetectionStrategy, Component, Inject, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { tap, withLatestFrom } from 'rxjs/operators';
import { takeUntilDestroyed } from '../../../rxjs-operators';
import { AuthState, SignIn, SIGN_IN_PAGE_URL } from '../../core/auth';
import { signedInTransition } from './animations';

@Component({
  templateUrl: './sign-in-page.component.html',
  styleUrls: ['./sign-in-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [signedInTransition]
})
export class SignInPageComponent implements OnDestroy {
  constructor(
    private store: Store,
    private router: Router,
    @Inject(SIGN_IN_PAGE_URL) private signInPageUrl: string,
    route: ActivatedRoute,
    fb: FormBuilder
  ) {
    this.form = fb.group({
      username: fb.control('', [Validators.required]),
      password: fb.control('', [Validators.required])
    });

    this.requestedUrl = route.snapshot.queryParamMap.get('requestedUrl');
  }

  busy = false;
  signedIn = false;
  form: FormGroup;

  private requestedUrl: string;

  // No-op method required to implement OnDestroy interface and use takeUntilDestroyed operator
  ngOnDestroy() {}

  signIn() {
    this.form.updateValueAndValidity();
    if (this.form.invalid) {
      return;
    }

    const { username, password } = this.form.value;
    this.busy = true;

    this.store
      .dispatch(new SignIn(username, password))
      .pipe(
        takeUntilDestroyed(this), // Tear down if component destroyed (page navigated away from)
        tap(() => (this.busy = false)), // Always unset busy flag after sign-in attempt
        withLatestFrom(this.store.select(AuthState.isAuthenticated)) // Determine if we're actually authenticated
      )
      .subscribe(({ 1: isAuthenticated }) => {
        this.signedIn = isAuthenticated;
      });
  }

  redirect(event: AnimationEvent) {
    if (event.toState) {
      this.router.navigateByUrl(this.redirectToUrl);
    }
  }

  private get redirectToUrl(): string {
    let redirectToUrl = '/';
    if (this.requestedUrl && !this.requestedUrl.startsWith(this.signInPageUrl)) {
      redirectToUrl = this.requestedUrl;
    }
    return redirectToUrl;
  }
}
