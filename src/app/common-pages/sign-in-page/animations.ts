import { transition, trigger, useAnimation } from '@angular/animations';
import { zoomOutRight } from 'ng-animate';

export const signedInTransition = trigger('signedInTransition', [
  transition('* => true', [useAnimation(zoomOutRight, { params: { timing: 0.3 } })])
]);
