import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule } from '@angular/material';
import { SharedModule } from '../shared';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { SignInPageComponent } from './sign-in-page/sign-in-page.component';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule, MatButtonModule, MatInputModule],
  declarations: [LandingPageComponent, NotFoundPageComponent, SignInPageComponent]
})
export class CommonPagesModule {}
