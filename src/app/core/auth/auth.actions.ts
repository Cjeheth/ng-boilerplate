export class SignIn {
  static readonly type = '[Auth] Sign-in';

  constructor(public username: string, public password: string) {}
}

export class SignOut {
  static readonly type = '[Auth] Sign-out';
}
