import { NgModule } from '@angular/core';
import { REDIRECT_ON_SIGN_OUT_BEHAVIOUR } from './behaviours/redirect-on-sign-out.service';

@NgModule({
  providers: [REDIRECT_ON_SIGN_OUT_BEHAVIOUR]
})
export class AuthModule {}
