import { inject, TestBed } from '@angular/core/testing';
import { NgxsModule } from '@ngxs/store';
import { AuthState } from '../auth.state';
import { IsNotAuthenticatedGuard } from './is-not-authenticated.guard';

describe('IsNotAuthenticatedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([AuthState])],
      providers: [IsNotAuthenticatedGuard]
    });
  });

  it('should be created', inject([IsNotAuthenticatedGuard], (service: IsNotAuthenticatedGuard) => {
    expect(service).toBeTruthy();
  }));
});
