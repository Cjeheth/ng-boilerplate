import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, NavigationExtras, Router, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthState } from '../auth.state';
import { SIGN_IN_PAGE_URL } from '../tokens';

@Injectable({
  providedIn: 'root'
})
export class IsAuthenticatedElseRedirectGuard implements CanActivate {
  constructor(private store: Store, private router: Router, @Inject(SIGN_IN_PAGE_URL) private signInUrl: string) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const isAuthenticated = this.store.selectSnapshot(AuthState.isAuthenticated);
    if (!isAuthenticated) {
      const extras = this.getExtrasForRequestedUrl(state.url);
      this.router.navigate([this.signInUrl], extras);
    }

    return isAuthenticated;
  }

  private getExtrasForRequestedUrl(requestedUrl: string): NavigationExtras {
    if (requestedUrl !== '/') {
      return {
        queryParams: { requestedUrl }
      };
    }

    return undefined;
  }
}
