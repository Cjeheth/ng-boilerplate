import { inject, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { AuthState } from '../auth.state';
import { SIGN_IN_PAGE_URL } from '../tokens';
import { IsAuthenticatedElseRedirectGuard } from './is-authenticated-else-redirect.guard';

describe('IsAuthenticatedElseRedirectGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([AuthState]), RouterTestingModule],
      providers: [IsAuthenticatedElseRedirectGuard, { provide: SIGN_IN_PAGE_URL, useValue: '/sign-in' }]
    });
  });

  it('should be created', inject([IsAuthenticatedElseRedirectGuard], (service: IsAuthenticatedElseRedirectGuard) => {
    expect(service).toBeTruthy();
  }));
});
