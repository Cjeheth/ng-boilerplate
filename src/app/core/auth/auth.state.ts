import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SignIn, SignOut } from './auth.actions';
import { AuthStateModel } from './auth.models';

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    isAuthenticated: false
  }
})
export class AuthState {
  @Selector()
  static isAuthenticated(state: AuthStateModel): boolean {
    return state.isAuthenticated;
  }

  @Action(SignIn)
  signIn(ctx: StateContext<AuthStateModel>, action: SignIn) {
    if (action.username === 'test' && action.password === 'test') {
      const state = ctx.getState();
      ctx.setState({
        ...state,
        isAuthenticated: true
      });
    }
  }

  @Action(SignOut)
  signOut(ctx: StateContext<AuthStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      isAuthenticated: false
    });
  }
}
