import { APP_INITIALIZER, forwardRef, Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, ofActionSuccessful } from '@ngxs/store';
import { noopFactory } from '../../../../utils/factories';
import { SignOut } from '../auth.actions';
import { SIGN_IN_PAGE_URL } from '../tokens';

export const REDIRECT_ON_SIGN_OUT_BEHAVIOUR = {
  provide: APP_INITIALIZER,
  useFactory: noopFactory,
  deps: [forwardRef(() => RedirectOnSignOutService)],
  multi: true
};

@Injectable({
  providedIn: 'root'
})
export class RedirectOnSignOutService {
  constructor(router: Router, private actions$: Actions, @Inject(SIGN_IN_PAGE_URL) signInUrl: string) {
    this.actions$.pipe(ofActionSuccessful(SignOut)).subscribe(() => {
      router.navigateByUrl(signInUrl);
    });
  }
}
