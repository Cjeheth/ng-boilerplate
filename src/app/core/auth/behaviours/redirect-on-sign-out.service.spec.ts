import { inject, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { AuthState } from '../auth.state';
import { SIGN_IN_PAGE_URL } from '../tokens';
import { RedirectOnSignOutService } from './redirect-on-sign-out.service';

describe('RedirectOnSignOutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([AuthState]), RouterTestingModule],
      providers: [RedirectOnSignOutService, { provide: SIGN_IN_PAGE_URL, useValue: '/sign-in' }]
    });
  });

  it('should be created', inject([RedirectOnSignOutService], (service: RedirectOnSignOutService) => {
    expect(service).toBeTruthy();
  }));
});
