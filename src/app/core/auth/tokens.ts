import { InjectionToken } from '@angular/core';

export const SIGN_IN_PAGE_URL = new InjectionToken<string>('URL of Sign-in page');
