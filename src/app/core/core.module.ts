import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsModule } from '@ngxs/store';
import { environment } from '../../environments/environment';
import { AuthModule, AuthState, SIGN_IN_PAGE_URL } from './auth';

export interface CoreModuleOptions {
  signInUrl: string;
}

@NgModule({
  imports: [
    NgxsModule.forRoot([AuthState]),
    NgxsRouterPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: environment.production
    }),
    AuthModule
  ]
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule = null
  ) {
    if (parentModule) {
      throw new Error('CoreModule should only be imported in root module.');
    }
  }

  static forRoot(options: CoreModuleOptions): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [{ provide: SIGN_IN_PAGE_URL, useValue: options.signInUrl }]
    };
  }
}
