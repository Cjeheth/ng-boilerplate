import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent, NotFoundPageComponent, SignInPageComponent } from './common-pages';
import { IsAuthenticatedElseRedirectGuard, IsNotAuthenticatedGuard } from './core/auth';

const routes: Routes = [
  {
    path: 'sign-in',
    component: SignInPageComponent,
    canActivate: [IsNotAuthenticatedGuard],
    data: { hideNav: true }
  },
  {
    path: '',
    canActivate: [IsAuthenticatedElseRedirectGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: LandingPageComponent
      },
      {
        path: 'examples',
        loadChildren: './features/examples#ExamplesModule'
      },
      {
        path: '**',
        component: NotFoundPageComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/sign-in'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
