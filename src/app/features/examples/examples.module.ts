import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared';
import { ExamplesPageComponent } from './examples-page/examples-page.component';
import { ExamplesRoutingModule } from './examples-routing.module';

@NgModule({
  imports: [SharedModule, ExamplesRoutingModule],
  declarations: [ExamplesPageComponent]
})
export class ExamplesModule {}
