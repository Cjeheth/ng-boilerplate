import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  templateUrl: './examples-page.component.html',
  styleUrls: ['./examples-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExamplesPageComponent {}
