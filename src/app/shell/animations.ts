import { query, state, style, transition, trigger, useAnimation } from '@angular/animations';
import { fadeIn, slideInUp, slideOutUp } from 'ng-animate';

export const pageTransition = trigger('pageTransition', [
  transition('* => *', [
    query(
      ':enter',
      [style({ position: 'absolute', width: '100%' }), useAnimation(fadeIn, { params: { timing: 0.3 } })],
      { optional: true }
    )
  ])
]);

export const navbarReveal = trigger('navbarReveal', [
  state('false', style({ transform: 'translateY(-150%)' })), // Translate off-screen + extra to hide shadow
  transition('false => true', [useAnimation(slideInUp, { params: { timing: 0.1 } })]),
  transition('true => false', [useAnimation(slideOutUp, { params: { timing: 0.1 } })])
]);
