import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material';
import { SharedModule } from '../shared';
import { ShellComponent } from './shell.component';

@NgModule({
  imports: [SharedModule, MatButtonModule],
  exports: [ShellComponent],
  declarations: [ShellComponent]
})
export class ShellModule {}
