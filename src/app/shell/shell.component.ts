import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { SignOut } from '../core/auth';
import { navbarReveal, pageTransition } from './animations';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [pageTransition, navbarReveal]
})
export class ShellComponent {
  constructor(private store: Store) {}

  @Input() showNav = false;
  @Input() contentChanged = false;

  signOut() {
    this.store.dispatch(new SignOut());
  }
}
