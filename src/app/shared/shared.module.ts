import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AutofocusDirective } from './directives/autofocus.directive';

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [CommonModule, RouterModule, AutofocusDirective],
  declarations: [AutofocusDirective]
})
export class SharedModule {}
